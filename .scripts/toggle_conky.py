#!/usr/bin/python3

import os
import sys
import subprocess
from json import dumps

# Fork magic abstraction to run in BG
def daemonize(func):
    try:
        pid = os.fork()
        if pid > 0:
            # Parent process, return and keep running
            return
    except OSError:
        sys.exit(1)

    os.setsid()

    # do second fork
    try:
        pid = os.fork()
        if pid > 0:
            # Exit from second parent
            sys.exit(0)
    except OSError:
        sys.exit(1)
    
    func()

    os._exit(os.EX_OK)

def runConky():
    subprocess.call(["conky"])

def killConky():
    subprocess.call(["killall", "conky"])




# Open the statusfile first.
with open('/home/nsuarez19/.scripts/etc/toggle_conky.status', "r") as f:
    status = f.read()
    print(dumps(status))

if(status == "1\n"):
    daemonize(runConky)
    with open('/home/nsuarez19/.scripts/etc/toggle_conky.status', 'w') as f:
        f.write("0\n") # Flip the flop
elif(status == "0\n"):
    daemonize(killConky)
    with open('/home/nsuarez19/.scripts/etc/toggle_conky.status', 'w') as f:
        f.write("1\n") # Flop the flip 
else:
    with open('/home/nsuarez19/.scripts/etc/toggle_conky.status', 'w') as f:
        f.write("1\n") # Default Init State
