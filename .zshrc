export ZSH=/home/nsuarez19/.oh-my-zsh

ZSH_THEME="ys"

source $ZSH/oh-my-zsh.sh

# User configuration
export LANG=en_US.UTF-8

HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000

bindkey -e

zstyle :compinstall filename '/home/nsuarez19/.zshrc'

autoload -Uz compinit
compinit

# Add $HOME/bin to path.
export PATH=$HOME/bin:$PATH

# Theme
ZSH_THEME="ys"

# Oh My ZSH plugins
plugins=(
    git
)

# Envvars
EDITOR=/usr/bin/vim

# Aliases
alias ls='ls --color=auto'
alias screenfetch='screenfetch -D arch'
alias zreload='source ~/.zshrc && echo "Reloaded ~/.zshrc"'
alias zshrc='vim ~/.zshrc'
alias chrome='nohup google-chrome-stable &;disown'
alias screenshot='nohup shutter &;disown'
alias firefox='nohup firefox &;disown'
alias pcmanfm='nohup pcmanfm &;disown'
alias rm='rm -I'
alias wifi='nmcli device wifi rescan;nmcli device wifi list'
alias connect='nmcli connection up'
alias cs='clear'
alias gitkraken='nohup gitkraken &;disown'
alias lock='nohup gnome-screensaver &;gnome-screensaver-command -l'
alias bconfig='/usr/bin/git --git-dir=$HOME/.dotfiles-repo --work-tree=$HOME'
alias bpush='/usr/bin/git --git-dir=$HOME/.dotfiles-repo --work-tree=$HOME push origin master'
alias foxit='nohup foxitreader &;disown'
alias music='nohup sonata &;disown'
alias filezilla='nohup filezilla &;disown'

# Therapy aliases
COW_FORTUNE='fortune -s | cowsay'
alias fuck=$COW_FORTUNE
alias shit=$COW_FORTUNE
alias why=$COW_FORTUNE
alias fuckyou=$COW_FORTUNE
