<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="Monty's Custom Startpage for Arch">
  <title>Start</title>

  <link rel="stylesheet" href="/rsc/pure-min.css">
  <link rel="stylesheet" href="/rsc/grids-responsive-min.css">
  <link rel="stylesheet" href="/rsc/main.css">
</head>

<body>

  <div id="layout" class="main-container">
    <div class="content main-content">
      <!--Date section-->
      <div class="text-center" id="weekday"></div>
      <div class="text-center" id="date"></div>

      <!-- Links section -->
      <div class="links-container" style="padding: 15%; padding-top: 15px">
        <div class="text-left links">
          <a class="link-main" href="https://reddit.com">Reddit</a> ::
          <a class="link-sub" href="https://reddit.com/r/Xenoblade_Chronicles">Xenoblade</a> :
          <a class="link-sub" href="https://reddit.com/r/dnd">D&amp;D</a> :
          <a class="link-sub" href="https://reddit.com/r/worldbuilding">Worldbuilding</a>
        </div>

        <div class="text-left links">
          <a class="link-main" href="https://github.com/montessquio">GitHub</a> ::
          <a class="link-sub" href="https://github.com/montessquio/pydir">pydir</a> :
          <a class="link-sub" href="https://github.com/Montessquio/DMG-Game">DMG-Game</a>
        </div>

        <div class="text-left links">
          <a class="link-main" href="#">Misc</a> ::
          <a class="link-sub" href="https://discordapp.net">Discord</a> :
          <a class="link-sub" href="https://dnd.guide">dnd.guide</a> :
          <a class="link-sub" href="https://thearcanum.club">The Arcanum</a>
        </div>

        <div class="text-left links">
          <a class="link-main" href="#">Docs</a> ::
          <a class="link-sub" href="https://wiki.archlinux.org">Arch Wiki</a> :
          <a class="link-sub" href="https://w3schools.com">W3Schools</a> :
          <a class="link-sub" href="https://stackoverflow.com">StackOverflow</a>
        </div>

        <div class="text-center" style="padding-top:40px">
            <?php
              $CONTENT = shell_exec("fortune -s");
              echo "\n";
              echo $CONTENT;
            ?>
        </div>
      </div>
      <div class="reddit-feed" style="padding: 20px; padding-top:0;">
        <h3 class="text-center">Reddit Feed</h3>
        <?php
        $url = "https://www.reddit.com/r/DnD+ProgrammerHumor+Xenoblade_Chronicles+gaming+hacking+programming+unixporn.rss?sort=new";
        $xml = simplexml_load_file($url);

         foreach($xml->children() as $post) // Iterate through elements
         {
           if ($post->getName() == 'entry') // If it's a reddit post...
            {
             echo '<div style="padding: 5%; margin-top: 10px; background-color: #808080; border-radius: 1px; color: black;" class="reddit-item">';
             echo '<a href="';
             echo $post->author->uri;
             echo '">';
             echo $post->author->name;
             echo '</a> in ';
             echo '<a href="';
             echo 'https://www.reddit.com/';
             echo $post->category['label'];
             echo '">';
             echo $post->category['label'];
             echo '</a><br />';
             echo '<h3><a href="';
             echo $post->link['href'];
             echo '">';
             echo $post->title;
             echo '</a></h3>';
             echo $post->content;
             echo '</div>';
            }
         }
         ?>
      </div>
    </div>
  </div>
  </div>
</div>

  <script type="text/javascript">
function weekDayToString(e){switch(e){case 1:return"Monday";case 2:return"Tuesday";case 3:return"Wednesday";case 4:return"Thursday";case 5:return"Friday";case 6:return"Saturday";case 0:return"Sunday";default:return"Error: weekDay number "+today.getDay()}}function monthToString(e){switch(e){case 0:return"January";case 1:return"February";case 2:return"March";case 3:return"April";case 4:return"May";case 5:return"June";case 6:return"July";case 7:return"August";case 8:return"September";case 9:return"October";case 10:return"November";case 11:return"December";default:return"Error in month conversion, number="+e}}function dayToString(e){switch(e){case 1:case 21:case 31:return e+"st";case 2:return e+"nd";case 3:return e+"rd";default:return e+"th"}}function writeDate(){var e=new Date,r=e.getMonth(),t=e.getDay(),n=e.getDate(),a=e.getFullYear();document.getElementById("weekday").innerHTML=weekDayToString(t),document.getElementById("date").innerHTML=monthToString(r)+" "+dayToString(n)+", "+a}function checkTime(e){return e<10&&(e="0"+e),e}writeDate();
  </script>
</body>
</html>
